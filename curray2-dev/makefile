CC = g++
CC_FLAGS = -std=c++17 -x c++ -m64
CC_LINK_FLAGS = -lpthread

NVCC = nvcc
PTXA_OPTIONS = -v
NVCC_FLAGS = -arch=sm_50 -m64 -std=c++17 -cudart static -ccbin $(CC)

BIN_DEBUG = x64/debug
BIN_RELEASE = x64/release
BIN_FILES = curray2 curray2-wl test plotWorldline.py shadow2D.py

DIR_CORE = core
DIR_CURRAY2_WL = curray2-wl
DIR_TEST = test

OUT_DEBUG_CORE = $(DIR_CORE)/$(BIN_DEBUG)
OUT_DEBUG_CURRAY2_WL = $(DIR_CURRAY2_WL)/$(BIN_DEBUG)
OUT_DEBUG_TEST = $(DIR_TEST)/$(BIN_DEBUG)

OUT_RELEASE_CORE = $(DIR_CORE)/$(BIN_RELEASE)
OUT_RELEASE_CURRAY2_WL = $(DIR_CURRAY2_WL)/$(BIN_RELEASE)
OUT_RELEASE_TEST = $(DIR_TEST)/$(BIN_RELEASE)

SCRIPTS = $(DIR_CORE)/src/plotWorldline.py $(DIR_CORE)/src/shadow2D.py

OUT_CORE =
OUT_CURRAY2_WL =
OUT_TEST =
BIN =
ifeq "$(findstring release, $(MAKECMDGOALS))" ""
	OUT_CORE = $(OUT_DEBUG_CORE)
	OUT_CURRAY2_WL = $(OUT_DEBUG_CURRAY2_WL)
	OUT_TEST = $(OUT_DEBUG_TEST)
	BIN = $(BIN_DEBUG)
	CC_FLAGS += -g -Og
	CC_LINKER_FLAGS += -g
else
	OUT_CORE = $(OUT_RELEASE_CORE)
	OUT_CURRAY2_WL = $(OUT_RELEASE_CURRAY2_WL)
	OUT_TEST = $(OUT_RELEASE_TEST)
	BIN = $(BIN_RELEASE)
	CC_FLAGS += -O3
endif

INC_GLOB = -I/usr/include -I/usr/local/include
INC_CORE = -I$(DIR_CORE)/src
INC_CURRAY2_WL = -I$(DIR_CURRAY2_WL)/src
INC_TEST = -I$(DIR_TEST)/src

LIB_GLOB = -L/usr/lib -L/usr/local/lib

__OBJS_CORE = kerr-newman/integrators.cu.o kerr-newman/metric.cu.o
OBJS_CORE = $(patsubst %, $(OUT_CORE)/%, $(__OBJS_CORE))
__OBJS_CORE_CUDA = kerr-newman/integrators.cu.co kerr-newman/metric.cu.co
OBJS_CORE_CUDA = $(patsubst %, $(OUT_CORE)/%, $(__OBJS_CORE_CUDA))

__OBJS_CURRAY2_WL = main.cpp.o worldline.cpp.o
OBJS_CURRAY2_WL = $(patsubst %, $(OUT_CURRAY2_WL)/%, $(__OBJS_CURRAY2_WL))

__OBJS_TEST = main.cpp.o
OBJS_TEST = $(patsubst %, $(OUT_TEST)/%, $(__OBJS_TEST))



#
# default rule
#

.PHONY: all
all: core core-cuda curray2-wl test scripts




#
# curray2 core
#

.PHONY: core
core: $(OBJS_CORE)

$(OUT_CORE)/%.o: $(DIR_CORE)/src/%
	mkdir -p $(dir $@)
	$(CC) $(CC_FLAGS) -c $(INC_GLOB) $(INC_CORE) $(LIB_GLOB) $< -o $@

.PHONY: core-cuda
core-cuda: $(OBJS_CORE_CUDA)

$(OUT_CORE)/%.co: $(DIR_CORE)/src/%
	mkdir -p $(dir $@)
	$(NVCC) $(NVCC_FLAGS) -dc $(INC_GLOB) $(INC_CORE) $(LIB_GLOB) $< -o $@




#
# curray2 worldline tool
#

.PHONY: curray2-wl
curray2-wl: create-bin-dir core $(OBJS_CURRAY2_WL)
	$(CC) $(CC_LINK_FLAGS) $(OBJS_CURRAY2_WL) $(OBJS_CORE) -o $(BIN)/curray2_wl

$(OUT_CURRAY2_WL)/%.o: $(DIR_CURRAY2_WL)/src/%
	mkdir -p $(dir $@)
	$(CC) $(CC_FLAGS) -c $(INC_GLOB) $(INC_CORE) $(INC_CURRAY2_WL) $(LIB_GLOB) $< -o $@




#
# curray test application
#

.PHONY: test
test: create-bin-dir core $(OBJS_TEST)
	$(CC) $(CC_LINK_FLAGS) $(OBJS_TEST) $(OBJS_CORE) -o $(BIN)/test

$(OUT_TEST)/%.o: $(DIR_TEST)/src/%
	mkdir -p $(dir $@)
	$(CC) $(CC_FLAGS) -c $(INC_GLOB) $(INC_CORE) $(INC_TEST) $(LIB_GLOB) $< -o $@




#
# curray2 scripts
#

.PHONY: scripts
scripts: create-bin-dir
	cp -f $(SCRIPTS) $(BIN)




.PHONY: debug
debug: all

.PHONY: release
release: all

.PHONY: create-bin-dir
create-bin-dir:
	mkdir -p $(BIN)

.PHONY: clean
clean:
	find $(OUT_DEBUG_CORE) $(OUT_DEBUG_CURRAY2_WL) $(OUT_DEBUG_TEST) -type f \( -iname \*.o -o -iname \*.co \) -exec rm -f -v {} + | true
	find $(OUT_RELEASE_CORE) $(OUT_RELEASE_CURRAY2_WL) $(OUT_RELEASE_TEST) -type f \( -iname \*.o -o -iname \*.co \) -exec rm -f -v {} + | true
	rm -f -v $(patsubst %, $(BIN_DEBUG)/%, $(BIN_FILES))
	rm -f -v $(patsubst %, $(BIN_RELEASE)/%, $(BIN_FILES))
