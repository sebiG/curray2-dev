import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mayavi import mlab
import json
import sys

def BL_to_cart(a, bl):
	t = bl[0]
	r = bl[1]
	theta = bl[2]
	phi = bl[3]

	return [
		t,
		np.sqrt(r**2 + a**2) * np.sin(theta) * np.cos(phi),
		np.sqrt(r**2 + a**2) * np.sin(theta) * np.sin(phi),
		r * np.cos(theta)
	]

m = float(sys.argv[1])
a = float(sys.argv[2])
q = float(sys.argv[3])
R = float(sys.argv[4])
filenames = sys.argv[5:]

plot_points = 1000

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.auto_scale_xyz([-R, R], [-R, R], [-R, R])
ax.set_xlim3d(-R, R)
ax.set_ylim3d(-R, R)
ax.set_zlim3d(-R, R)

# mayavis figure
mlab.figure(size=(1000,1000), bgcolor=(1,1,1))

# plot black hole event horizon
r_bh = m + np.sqrt(m**2 - a**2 - q**2)
u_bh = np.linspace(0, 2 * np.pi, 20)
v_bh = np.linspace(0, np.pi, 20)
x_bh = np.sqrt(r_bh**2 + a**2) * np.outer(np.cos(u_bh), np.sin(v_bh))
y_bh = np.sqrt(r_bh**2 + a**2) * np.outer(np.sin(u_bh), np.sin(v_bh))
z_bh = r_bh * np.outer(np.ones(np.size(u_bh)), np.cos(v_bh))
mlab.mesh(x_bh, y_bh, z_bh, color=(0.0, 0.0, 0.0), opacity=0.5)

# colors
colors = [
	(1, 0, 0),
	(0, 1, 0),
	(0, 0, 1),
	(1, 1, 0),
	(0, 1, 1),
	(1, 0, 1)
]

# plot worldlines
count = 0
for filename in filenames:
	file = open(filename)
	data = np.array(json.load(file))[1:].astype('float64')

	# data is in the form
	# t, r, theta, phi, v_t, v_r, v_theta, v_phi, a_r, a_theta
	step = int(np.ceil(plot_points / len(data)))
	worldline = (data[0::step]).transpose().reshape((10, -1))
	worldlineCart = BL_to_cart(a, worldline)

	mlab.plot3d(worldlineCart[1], worldlineCart[2], worldlineCart[3], color=colors[count])

	count += 1

mlab.show()