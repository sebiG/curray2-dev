import numpy as np
import matplotlib
import matplotlib.colors
import matplotlib.pyplot as plt
import json
import sys
import os
import shutil

def horizon_r(m, a, q):
	return m + np.sqrt(m**2 - a**2 - q**2)

# bl = [t, r, theta, phi]
# return = [t, x, y, z]
def BL_to_cart(a, bl):
	t = bl[0]
	r = bl[1]
	theta = bl[2]
	phi = bl[3]

	return [
		t,
		np.sqrt(r**2 + a**2) * np.sin(theta) * np.cos(phi),
		np.sqrt(r**2 + a**2) * np.sin(theta) * np.sin(phi),
		r * np.cos(theta)
	]

# cart = [t, x, y, z=0]
# return = [t, r, theta, phi]
def eq_cart_to_BL(a, cart):
	t = cart[0]
	x = cart[1]
	y = cart[2]

	r = np.sqrt(x**2 + y**2 - a**2)
	theta = np.pi / 2.0
	phi = np.arctan2(cart[2], cart[1]); # phi = atan(y / x)

	return [
		t,
		r,
		theta,
		phi
	]

# bl_pos = [t, r, theta=pi/2, phi]
# cart_vec = [v_x, v_y, v_z=0]
# return = [v_r, v_theta=0, v_phi]
def eq_cart_to_BL_vec(a, bl_pos, cart_vec):
	r_ = np.sqrt(bl_pos[1]**2 + a**2)

	# Einheitsvektoren
	# e_r_ = cos(phi) e_x + sin(phi) e_y
	# e_phi = -sin(phi) e_x + cos(phi) e_y
	# e_x = cos(phi) e_r_ - sin(phi) e_phi
	# e_y = sin(phi) e_r_ + cos(phi) e_phi

	# Tangentenvektoren
	# t_r = r/r_ e_r_
	# t_phi = e_phi * r_
	# e_x = cos(phi) * r_/r * t_r - sin(phi) / r_ * t_phi
	# e_y = sin(phi) * r_/r * t_r + cos(phi) / r_ * t_phi
	
	# Umrechnungen
	# v_x e_x + v_y e_y
	#  = [v_x cos(phi) * r_/r + v_y sin(phi) * r_/r] t_r
	#    + [-v_x sin(phi) / r_ + v_y cos(phi) / r_] t_phi

	vec_r = cart_vec[0] * np.cos(bl_pos[3]) * r_ / bl_pos[1]
	vec_r += cart_vec[1] * np.sin(bl_pos[3]) * r_ / bl_pos[1]

	vec_phi = -cart_vec[0] / r_ * np.sin(bl_pos[3])
	vec_phi += cart_vec[1] / r_ * np.cos(bl_pos[3])

	return [
		vec_r,
		0,
		vec_phi
	]

m = float(sys.argv[1])
a = float(sys.argv[2])
q = float(sys.argv[3])

x0 = float(sys.argv[4])
y0_1 = float(sys.argv[5])
y0_2 = float(sys.argv[6])
n = int(sys.argv[7])

x_limit = np.abs(x0)
y_limit = np.max([np.abs(y0_1), np.abs(y0_2)])

plt.figure();
plt.axes().set_aspect('equal')
plt.axes().set_xlim(-x_limit, x_limit)
plt.axes().set_ylim(-y_limit, y_limit)

# light rays initial x and y coordinates
y0_ = np.linspace(y0_1, y0_2, n)
# light rays initial velocity
vx0 = 1
vy0 = 0

# create output directory
shutil.rmtree("shadow2D_out");
if not os.path.isdir("shadow2D_out"):
	os.makedirs("shadow2D_out")

count = 0
for y0 in y0_:
	pos_bl = eq_cart_to_BL(a, [0, x0, y0, 0])
	vel3_bl = eq_cart_to_BL_vec(a, pos_bl, [vx0, vy0, 0])
	#print(pos_bl)
	#print(vel3_bl)

	args = "cuRRay2-wl"
	if os.name != "nt" and not os.path.isfile("cuRRay2-wl"):
		args = "./cuRRay2-wl.exe" # for wsl
	args += " -M " + str(m) + " -a " + str(a) + " -Q " + str(q) + " -m 0"
	args += " -p \"" + str(pos_bl[0]) + " " + str(pos_bl[1]) + " "
	args += str(pos_bl[2]) + " " + str(pos_bl[3]) + "\""
	args += " -v \"" + str(vel3_bl[0]) + " " + str(vel3_bl[1]) + " "
	args += str(vel3_bl[2]) + "\""
	args += " -s " + str(np.sqrt(x_limit**2 + y_limit**2 + 1))
	args += " -e 1e-3"
	args += " -i \"DP45\" -t 1e-9 -n 500000 -f tmp.json"

	print(args)
	os.system(args)

	file = open("tmp.json")
	data = np.array(json.load(file))[1:].astype('float64')
	shutil.copyfile("tmp.json", "shadow2D_out/" + str(count) + ".json")

	# data is in the form
	# t, r, theta, phi, v_t, v_r, v_theta, v_phi, a_r, a_theta
	worldline = data.transpose().reshape((10, -1))
	worldlineCart = BL_to_cart(a, worldline)

	# add worldline
	color_percent = (count + 1) / n
	c = matplotlib.colors.hsv_to_rgb([color_percent*0.9, 1, 0.8])
	plt.plot(worldlineCart[1], worldlineCart[2], color=c)

	# output raw dat file for latex plotting
	max_points = 100
	stride = len(worldlineCart[0]) // max_points
	file = open("shadow2D_out/" + str(count) + "_cart.dat", "w")
	file.write("t x y z\n")
	for i in range(0, max_points):
		file.write(str(worldlineCart[0][i * stride]))
		file.write(" ")
		file.write(str(worldlineCart[1][i * stride]))
		file.write(" ")
		file.write(str(worldlineCart[2][i * stride]))
		file.write(" ")
		file.write(str(worldlineCart[3][i * stride]))
		file.write('\n')

	# increment count
	count += 1

# draw event horizon
circle_radius = np.sqrt(horizon_r(m, a, q)**2 + a**2)
circle = plt.Circle((0,0), radius=circle_radius, color='black')
plt.gca().add_patch(circle)

plt.show()