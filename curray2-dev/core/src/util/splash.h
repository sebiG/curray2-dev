//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#ifndef CURRAY2_UTIL_SPLASH_H
#define CURRAY2_UTIL_SPLASH_H

#include <string>

namespace cuRRay2 {

std::string const VERSION = "0.1";
std::string const HLINE =
"------------------------------------------------------------------";
std::string const BANNER =
R"(                       ____     ____                 ___ 
        _______  __   / __ \   / __ \____ ___  __   |__ \
       / ___/ / / /  / /_/ /  / /_/ / __ `/ / / /   __/ /
      / /__/ /_/ /  / _, _/  / _, _/ /_/ / /_/ /   / __/ 
      \___/\__,_/  /_/ |_|  /_/ |_|\__,_/\__, /   /____/ 
                                        /____/           
      
        CUDA(R)  Relativistic    Raytracer

                       Version )" + VERSION +
	R"(
                         ,
               (c) 2020 Sebastien Garmier
)";

}

#endif