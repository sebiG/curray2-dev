//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#ifndef CURRAY2_UTIL_MATH_H
#define CURRAY2_UTIL_MATH_H

#include <type_traits>
#include <cmath>

#include "default.h"

namespace cuRRay2 {

#define MATH_PI 3.1415926535897932384626433832795
#define MATH_TWO_PI 6.283185307179586476925286766559
#define MATH_PI_OVER_TWO 1.5707963267948966192313216916398
#define MATH_PI_OVER_FOUR 0.78539816339744830961566084581988
#define MATH_PI_OVER_EIGHT 0.39269908169872415480783042290994
#define MATH_ONE_OVER_PI 0.31830988618379067153776752674503
#define MATH_ONE_OVER_TWO_PI 0.15915494309189533576888376337251

#define MATH_E 2.7182818284590452353602874713527
#define MATH_ONE_OVER_E 0.36787944117144232159552377016146

#define MATH_LN_2 0.69314718055994530941723212145818
#define MATH_LN_10 2.3025850929940456840179914546844

#define MATH_SQRT_2 1.4142135623730950488016887242097
#define MATH_ONE_OVER_SQRT_2 0.70710678118654752440084436210485

CUDA_HOST_DEVICE
template <typename T> int
static constexpr sgn(T val) {
    static_assert(std::is_arithmetic_v<T>, "typename must be arithmetic");

    return (T(0) < val) - (val < T(0));
}

CUDA_HOST_DEVICE
template <typename T> T 
static constexpr sqr(T val) {
    static_assert(std::is_arithmetic_v<T>, "typename must be arithmetic");

    return val * val;
}

CUDA_HOST_DEVICE
template <unsigned power, typename T> 
static constexpr T pow(T val) {
    static_assert(std::is_arithmetic_v<T>, "typename must be arithmetic");

    T ret = T(0);
    for (unsigned i = 0; i < power; ++i) {
        ret *= val;
    }
    return val;
}

CUDA_HOST_DEVICE
static void sin_cos(double val, double* s, double* c) {
#ifdef __CUDA_ARCH__
    sincos(val, s, c);
#else
    *s = sin(val);
    *c = cos(val);
#endif
}

}

#endif
