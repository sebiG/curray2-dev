//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#include "kerr-newman/metric.h"
#include "util/math.h"

#ifndef __CUDA_ARCH__
#include <iostream>
#endif

using namespace cuRRay2;
using namespace SimpleVec;

CUDA_HOST_DEVICE
Vector4_double KerrNewman::bl_to_cart(
	KerrNewman::BlackHole const& bh,
	Vector4_double const& bl_pos
) {
	Vector4_double cart_pos;

	double s = sqrt(sqr(bl_pos.m_1) + sqr(bh.a));
	double sin_theta, cos_theta, sin_phi, cos_phi;

	sin_cos(bl_pos.m_2, &sin_theta, &cos_theta);
	sin_cos(bl_pos.m_3, &sin_phi, &cos_phi);

	cart_pos.m_0 = bl_pos.m_0;
	cart_pos.m_1 = s * sin_theta * sin_phi;
	cart_pos.m_2 = s * sin_theta * cos_phi;
	cart_pos.m_3 = bl_pos.m_1 * cos_theta;

	return cart_pos;
}

CUDA_HOST_DEVICE
void KerrNewman::compute_metric_components(
	BlackHole const& bh,
	Vector4_double const& pos,
	MetricComponents* g
) {
	// shorthands
	double cos2_theta = sqr(cos(pos.m_2));
	double sin2_theta = 1.0 - cos2_theta;

	double r2 = sqr(pos.m_1);
	double a2 = sqr(bh.a);

	double rho2 = r2 + a2 * cos2_theta;
	double delta = r2 - 2.0 * bh.M * pos.m_1 + a2 + sqr(bh.Q);

	// calculate the components
	g->g00 = -(delta - a2 * sin2_theta) / rho2;
	g->g11 = rho2 / delta;
	g->g22 = rho2;
	g->g33 = sin2_theta / rho2 * (sqr(r2 + a2) - delta * a2 * sin2_theta);
	g->g03 = -bh.a * sin2_theta * (r2 + a2 - delta) / rho2;
}

CUDA_HOST_DEVICE
double KerrNewman::compute_outer_horizon_r(
	BlackHole const& bh
) {
	return bh.M + sqrt(sqr(bh.M) - sqr(bh.Q) - sqr(bh.a));
}

CUDA_HOST_DEVICE
double KerrNewman::compute_norm_2(
	MetricComponents const& g,
	Vector4_double const& vec
) {
	return g.g00 * sqr(vec.m_0)
		+ g.g11 * sqr(vec.m_1)
		+ g.g22 * sqr(vec.m_2)
		+ g.g33 * sqr(vec.m_3)
		+ 2.0 * g.g03 * vec.m_0 * vec.m_3;
}

CUDA_HOST_DEVICE
Vector4_double KerrNewman::compute_covector(
	MetricComponents const& g,
	Vector4_double const& vec
) {
	Vector4_double cov;
	cov.m_0 = g.g00 * vec.m_0 + g.g03 * vec.m_3;
	cov.m_1 = g.g11 * vec.m_1;
	cov.m_2 = g.g22 * vec.m_2;
	cov.m_3 = g.g03 * vec.m_0 + g.g33 * vec.m_3;

	return cov;
}

CUDA_HOST_DEVICE
double KerrNewman::normalize_t_component(
	Vector4_double const& vec,
	KerrNewman::MetricComponents const& g,
	double norm2
) {
	// norm2 = g_00 vec_0^2 + 2 g_03 vec_0 vec_3
	//     + g_11 vec_1^2 + g_22 vec_2^2 + g_33 vec_3^2
	//
	// vec_0 = (-2 g_03 vec_3 - sqrt{
	//    4 g_03^2 vec_3^2
	//    - 4 g_00 [g_11 vec_1^2 + g_22 vec_2^2 + g_33 vec_3^2 - norm2]
	// }) / (2 g_00)

	double d = 4.0 * sqr(g.g03 * vec.m_3)
	-4.0 * g.g00 * 
		(g.g11 * sqr(vec.m_1) + g.g22 * sqr(vec.m_2) + g.g33 * sqr(vec.m_3)
		- norm2);

	return (-2.0 * g.g03 * vec.m_3 - sqrt(d)) / (2.0 * g.g00);
}

CUDA_HOST_DEVICE
void KerrNewman::compute_motion_constants(
	BlackHole const& bh,
	double r, double theta,
	ParticleConstants const& pc,
	Vector4_double const& p_cov,
	MotionConstants* mc
) {
	// following Misner, Thonre, Wheeler (1970)

	// shorthands
	double cos2_theta = sqr(cos(theta));
	double sin2_theta = 1.0 - cos2_theta;

	double a2 = sqr(bh.a);

	double rho2 = sqr(r) + a2 * cos2_theta;

	double Qr_rho2 = bh.Q * r / rho2;

	// components of electromagnetic potential
	double A_0 = -Qr_rho2;
	double A_3 = Qr_rho2 * bh.a * sin2_theta;

	// energy at infinity
	mc->E = -p_cov.m_0 - pc.e * A_0;

	// axial angular momentum at infinity
	mc->Lz = p_cov.m_3 + pc.e * A_3;

	// Carter's constant
	mc->C = sqr(p_cov.m_2) + cos2_theta 
		* (a2 * (sqr(pc.mu) - sqr(mc->E)) + sqr(mc->Lz) / sin2_theta);
}

CUDA_HOST_DEVICE
void KerrNewman::compute_RHS(
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	double r, double theta,
	double v_r, double v_theta,
	double* v_t, double* v_phi,
	double* v_r2, double* v_theta2,
	double* a_r, double* a_theta
) {
	// first part following Misner, Thorne, Wheeler (1970)

	// shorthands
	double sin_theta, cos_theta;
	sin_cos(theta, &sin_theta, &cos_theta);

	double cos2_theta = sqr(cos_theta);
	double sin2_theta = 1.0 - cos2_theta;

	double r2 = sqr(r);
	double a2 = sqr(bh.a);

	double rho2 = r2 + a2 * cos2_theta;
	double delta = r2 - 2.0 * bh.M * r + a2 + sqr(bh.Q);

	// calculate velocity t and phi components
	double P = mc.E * (r2 + a2) - mc.Lz * bh.a - pc.e * bh.Q * r;

	*v_t = -bh.a * (bh.a * mc.E * sin2_theta - mc.Lz) + (r2 + a2) * P / delta;
	*v_t /= rho2;
	*v_phi = -(bh.a * mc.E - mc.Lz / sin2_theta) + bh.a * P / delta;
	*v_phi /= rho2;

	// calculate squares of r and theta velcity components
	double mu2 = sqr(pc.mu);
	double Lz2 = sqr(mc.Lz);
	double E2 = sqr(mc.E);
	double rho2_2 = sqr(rho2);
	double Lz_aE = mc.Lz - bh.a * mc.E;

	double R = sqr(P) - delta * (mu2 * r2 + sqr(Lz_aE) + mc.C);
	double Theta = mc.C - cos2_theta * (a2 * (mu2 - E2) + Lz2 / sin2_theta);

	*v_r2 = R / rho2_2;
	*v_theta2 = Theta / rho2_2;

	// rest following Sharp (1981)

	// calculate r and theta acceleration components
	double drho2 = 2.0 * r * v_r - a2 * sin_theta * cos_theta * v_theta;

	*a_r = P * (2 * r * mc.E - pc.e * bh.Q)
		- (mu2 * r2 + sqr(Lz_aE) + mc.C) * (r - bh.M)
		- mu2 * r * delta
		- v_r * rho2 * drho2;
	*a_theta = (mu2 - E2) * a2 * cos_theta * sin_theta
		+ Lz2 * cos_theta / (sin_theta * sin2_theta)
		- v_theta * rho2 * drho2;
	*a_r /= rho2_2;
	*a_theta /= rho2_2;
}

// Christoffel symbols following Frolov, Novikov (1998)

// in order not to repeat everything four times, use macros

#define CURRAY2_KERRNEWMAN_CHRISTOFFEL_SH					\
double sin_theta, cos_theta;								\
sin_cos(theta, &sin_theta, &cos_theta);						\
															\
double cos2_theta = sqr(cos_theta);							\
double sin2_theta = 1.0 - cos2_theta;						\
															\
double r2 = sqr(r);											\
double a2 = sqr(bh.a);										\
double Q2 = sqr(bh.Q);										\
															\
double rho2 = r2 + a2 * cos2_theta;							\
double delta = r2 - 2.0 * bh.M * r + a2 + Q2;				\
															\
double b2 = r2 + a2;										\
double p = (r * Q2 + bh.M * (rho2 - 2.0 * r2)) / sqr(rho2);	\
double q = (Q2 - 2.0 * bh.M * r) / (2.0 * rho2);

#define CURRAY2_KERRNEWMAN_CHRISTOFFEL_0						\
cs0->G001 = -p * b2 / delta;									\
cs0->G013 = bh.a * sin2_theta * (p * b2 + 2.0 * q * r) / delta;	\
cs0->G020 = q * a2 * sin_2theta / rho2;							\
cs0->G032 = -q * a2 * bh.a * sin2_theta * sin_2theta / rho2;	\

#define CURRAY2_KERRNEWMAN_CHRISTOFFEL_1							\
cs1->G100 = -delta * p / rho2;										\
cs1->G103 = bh.a * delta * p * sin2_theta / rho2;					\
cs1->G111 = r / rho2 + (bh.M - r) / delta;							\
cs1->G112 = -a2 * sin_2theta / (2.0 * rho2);						\
cs1->G122 = -r * delta / rho2;										\
cs1->G133 = -delta * sin2_theta * (r + p * a2 * sin2_theta) / rho2;	\

#define CURRAY2_KERRNEWMAN_CHRISTOFFEL_2								\
cs2->G200 = q * a2 * sin_2theta / rho2;									\
cs2->G203 = -q * bh.a * b2 * sin_2theta / sqr(rho2);					\
cs2->G211 = a2 * sin_2theta / (2.0 * rho2);								\
cs2->G212 = r / rho2;													\
cs2->G222 = -a2 * sin_2theta / (2.0 * rho2);							\
cs2->G233 = -sin_2theta / (2.0 * rho2)									\
* (b2 - 2.0 * a2 * sin2_theta * q * (2.0 + a2 * sin2_theta / rho2));	\

#define CURRAY2_KERRNEWMAN_CHRISTOFFEL_3								\
cs3->G301 = -bh.a * p / delta;											\
cs3->G302 = 2.0 * q * bh.a * cot_theta / rho2;							\
cs3->G313 = r * (1 + 2.0 * q) / delta + a2 * p * sin2_theta / delta;	\
cs3->G332 = cot_theta / delta * (										\
	(1 + 2.0 * q) * (b2 - 2.0 * q * a2 * sin2_theta)					\
	- 2.0 * q * a2 * b2 * sin2_theta / rho2);

CUDA_HOST_DEVICE
void KerrNewman::compute_christoffel_symbols(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_0* cs0,
	ChristoffelSymbols_1* cs1,
	ChristoffelSymbols_2* cs2,
	ChristoffelSymbols_3* cs3
) {
	// shorthands
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_SH;
	double sin_2theta = 2.0 * sin_theta * cos_theta;
	double cot_theta = cos_theta / sin_theta;

	CURRAY2_KERRNEWMAN_CHRISTOFFEL_0;
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_1;
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_2;
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_3;
}

void KerrNewman::compute_christoffel_symbols_0(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_0* cs0
) {
	// shorthands
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_SH;
	double sin_2theta = 2.0 * sin_theta * cos_theta;

	// calculate symbols
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_0;
}

void KerrNewman::compute_christoffel_symbols_1(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_1* cs1
) {
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_SH;
	double sin_2theta = 2.0 * sin_theta * cos_theta;

	// calculate symbols
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_1;
}

void KerrNewman::compute_christoffel_symbols_2(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_2* cs2
) {
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_SH;
	double sin_2theta = 2.0 * sin_theta * cos_theta;

	// calculate symbols
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_2;
}

void KerrNewman::compute_christoffel_symbols_3(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_3* cs3
) {
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_SH;
	double cot_theta = cos_theta / sin_theta;

	// calculate symbols
	CURRAY2_KERRNEWMAN_CHRISTOFFEL_3;
}

#undef CURRAY2_KERRNEWMAN_CHRISTOFFEL_SH
#undef CURRAY2_KERRNEWMAN_CHRISTOFFEL_0
#undef CURRAY2_KERRNEWMAN_CHRISTOFFEL_1
#undef CURRAY2_KERRNEWMAN_CHRISTOFFEL_2
#undef CURRAY2_KERRNEWMAN_CHRISTOFFEL_3