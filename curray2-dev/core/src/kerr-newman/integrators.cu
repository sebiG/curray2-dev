//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#include "kerr-newman/integrators.h"

#include <algorithm>
#ifndef __CUDA_ARCH__
#include <iostream>
#endif

using namespace std;

using namespace cuRRay2;
using namespace SimpleVec;

CUDA_HOST_DEVICE
double KerrNewman::heuristic_step_size_estimator(
	Vector4_double const& pos,
	Vector4_double const& vel
) {
	double _0 = abs(1.0 / vel.m_0);
	double _1 = abs(1.0 / vel.m_1);
	double _2 = abs(1.0 / vel.m_2);
	double _3 = abs(1.0 / vel.m_3);

	return KerrNewman::BASIC_STEP_SIZE_MULTIPLIER
		* min(min(min(_0, _1), _2), _3);
}

// Runge-Kutta 4 with basic step size control
CUDA_HOST_DEVICE
double KerrNewman::heuristic_RK4_step(
	KerrNewman::BlackHole const& bh,
	KerrNewman::MotionConstants const& mc,
	KerrNewman::ParticleConstants const& pc,
	Vector4_double* pos,
	double* v_r, double* v_theta
) {
	double dummy;

	// first substep
	double v_t_1, v_phi_1, a_r_1, a_theta_1;
	double v_r_1 = *v_r;
	double v_theta_1 = *v_theta;
	KerrNewman::compute_RHS(
		bh, mc, pc,
		pos->m_1, pos->m_2,
		v_r_1, v_theta_1,
		&v_t_1, &v_phi_1,
		&dummy, &dummy,
		&a_r_1, &a_theta_1
	);

	// get step size
	double h = KerrNewman::heuristic_step_size_estimator(
		*pos,
		{ v_t_1, v_r_1, v_theta_1, v_phi_1 }
	);

	// second substep
	double v_t_2, v_phi_2, a_r_2, a_theta_2;
	double v_r_2 = *v_r + 0.5 * h * a_r_1;
	double v_theta_2 = *v_theta + 0.5 * h * a_theta_1;
	KerrNewman::compute_RHS(
		bh, mc, pc,
		pos->m_1 + 0.5 * h * v_r_1,
		pos->m_2 + 0.5 * h * v_theta_1,
		v_r_2, v_theta_2,
		&v_t_2, &v_phi_2,
		&dummy, &dummy,
		&a_r_2, &a_theta_2
	);

	// third substep
	double v_t_3, v_phi_3, a_r_3, a_theta_3;
	double v_r_3 = *v_r + 0.5 * h * a_r_2;
	double v_theta_3 = *v_theta + 0.5 * h * a_theta_2;
	KerrNewman::compute_RHS(
		bh, mc, pc,
		pos->m_1 + 0.5 * h * v_r_2,
		pos->m_2 + 0.5 * h * v_theta_2,
		v_r_3, v_theta_3,
		&v_t_3, &v_phi_3,
		&dummy, &dummy,
		&a_r_3, &a_theta_3
	);

	// fourth substep
	double v_t_4, v_phi_4, a_r_4, a_theta_4;
	double v_r_4 = *v_r + h * a_r_3;
	double v_theta_4 = *v_theta + h * a_theta_3;
	KerrNewman::compute_RHS(
		bh, mc, pc,
		pos->m_1 + h * v_r_3,
		pos->m_2 + h * v_theta_3,
		v_r_4, v_theta_4,
		&v_t_4, &v_phi_4,
		&dummy, &dummy,
		&a_r_4, &a_theta_4
	);

	// do step
	*v_r += h * (a_r_1 + 2.0 * a_r_2 + 2.0 * a_r_3 + a_r_4) / 6.0;
	*v_theta +=
		h * (a_theta_1 + 2.0 * a_theta_2 + 2.0 * a_theta_3 + a_theta_3) / 6.0;

	*pos += h * (
		Vector4_double{ v_t_1, v_r_1, v_theta_1, v_phi_1 }
		+ 2.0 * Vector4_double{v_t_2, v_r_2, v_theta_2, v_phi_2 }
		+ 2.0 * Vector4_double{ v_t_3, v_r_3, v_theta_3, v_phi_3 }
		+ Vector4_double{ v_t_4, v_r_4, v_theta_4, v_phi_4 }
	) / 6.0;

	// return step size
	return h;
}

CUDA_HOST_DEVICE
bool KerrNewman::cart_check_adaptive_RK_step(
	KerrNewman::BlackHole const& bh,
	Vector4_double const& pos,
	Vector4_double const& delta_pos,
	Vector4_double const& low_delta_pos,
	double abs_tol, double rel_tol
) {

	Vector4_double cart_new_pos =
		KerrNewman::bl_to_cart(bh, *pos + delta_pos);
	Vector4_double cart_new_low_pos = 
		KerrNewman::bl_to_cart(bh, *pos + low_delta_pos);

	double error2 = (cart_new_pos - cart_new_low_pos).Length2();

	// TODO: implement relative tolerance
	bool accept = error2 < abs_tol * abs_tol;

	return accept;
}

#ifndef __CUDA_ARCH__

string KerrNewman::print_adaptive_integrator_short_names() {
	std::string ret = "";
	for (unsigned i = 0;
		i < KerrNewman::adaptive_RK_integrators_count - 1; ++i
	) {
		ret +=
			"\"" + KerrNewman::adaptive_RK_integrators[i].short_name + "\", ";
	}
	ret += KerrNewman::adaptive_RK_integrators
		[KerrNewman::adaptive_RK_integrators_count - 1].short_name;
	return ret;
}

bool KerrNewman::adaptive_RK_step(
	unsigned method,
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult,
	double reject_mult
) {
	switch (method) {
	case EH12:
		return KerrNewman::adaptive_EH12_step(
			bh, mc, pc,
			pos, v_r, v_theta,
			h, abs_tol, accept_mult, reject_mult
		);
	case BS23:
		return KerrNewman::adaptive_BS23_step(
			bh, mc, pc,
			pos, v_r, v_theta,
			h, abs_tol, accept_mult, reject_mult
		);
	case RKF45:
		return KerrNewman::adaptive_RKF45_step(
			bh, mc, pc,
			pos, v_r, v_theta,
			h, abs_tol, accept_mult, reject_mult
		);
	case CK45:
		return KerrNewman::adaptive_CK45_step(
			bh, mc, pc,
			pos, v_r, v_theta,
			h, abs_tol, accept_mult, reject_mult
		);
	case DP45:
		return KerrNewman::adaptive_DP45_step(
			bh, mc, pc,
			pos, v_r, v_theta,
			h, abs_tol, accept_mult, reject_mult
		);
	default:
		return false;
	}
}

#endif

CUDA_HOST_DEVICE
bool KerrNewman::adaptive_EH12_step(
	KerrNewman::BlackHole const& bh,
	KerrNewman::MotionConstants const& mc,
	KerrNewman::ParticleConstants const& pc,
	Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult,
	double reject_mult
) {

	// butcher tableau
	const double A[] = { 1.0 };
	const double C2[] = { 0.5, 0.5 };
	const double C1[] = { 1.0, 0.0 };

	return KerrNewman::raw_adaptive_RK_step<2>(
		A, C1, C2,
		bh, mc, pc,
		pos,
		v_r, v_theta,
		h,
		abs_tol,
		accept_mult, reject_mult
	);

}

CUDA_HOST_DEVICE
bool KerrNewman::adaptive_BS23_step(
	KerrNewman::BlackHole const& bh,
	KerrNewman::MotionConstants const& mc,
	KerrNewman::ParticleConstants const& pc,
	Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult,
	double reject_mult
) {

	// butcher tableau
	const double A[] = {
		0.5       ,
		0.0       , 3.0 / 4.0 ,
		2.0 / 9.0 , 1.0 / 3.0 , 4.0 / 9.0
	};
	const double C3[] = {
		2.0 / 9.0,
		1.0 / 3.0,
		4.0 / 9.0,
		0.0
	};
	const double C2[] = {
		7.0 / 24.0,
		1.0 / 4.0,
		1.0 / 3.0,
		1.0 / 8.0
	};

	return KerrNewman::raw_adaptive_RK_step<4>(
		A, C2, C3,
		bh, mc, pc,
		pos,
		v_r, v_theta,
		h,
		abs_tol,
		accept_mult, reject_mult
	);

}

CUDA_HOST_DEVICE
bool KerrNewman::adaptive_RKF45_step(
	KerrNewman::BlackHole const& bh,
	KerrNewman::MotionConstants const& mc,
	KerrNewman::ParticleConstants const& pc,
	Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult,
	double reject_mult
) {

	// butcher tableau
	const double A[] = {
		1.0 / 4.0,
		3.0 / 32.0      , 9.0 / 32.0       ,
		1932.0 / 2197.0 , -7200.0 / 2197.0 , 7296.0 / 2197.0  ,
		439.0 / 216.0   , -8.0             , 3680.0 / 513.0   , -845.0 / 4104.0 ,
		-8.0 / 27.0     , 2.0              , -3544.0 / 2565.0 , 1859.0 / 4104.0 , -11.0 / 40.0
	};
	const double C5[] = {
		16.0 / 135.0,
		0.0,
		6656.0 / 12825.0,
		28561.0 / 56430.0,
		-9.0 / 50.0,
		2.0 / 55.0
	};
	const double C4[] = {
		25.0 / 216.0,
		0.0,
		1408.0 / 2565.0,
		1408.0 / 2565.0,
		-1.0 / 5.0,
		0.0
	};

	return KerrNewman::raw_adaptive_RK_step<6>(
		A, C4, C5,
		bh, mc, pc,
		pos,
		v_r, v_theta,
		h,
		abs_tol,
		accept_mult, reject_mult
	);

}

CUDA_HOST_DEVICE
bool KerrNewman::adaptive_CK45_step(
	KerrNewman::BlackHole const& bh,
	KerrNewman::MotionConstants const& mc,
	KerrNewman::ParticleConstants const& pc,
	Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult,
	double reject_mult
) {

	// butcher tableau
	const double A[] = {
		1.0 / 5.0,
		3.0 / 40.0       , 9.0 / 40.0    ,
		3.0 / 10.0       , -9.0 / 10.0   , 6.0 / 5.0       ,
		-11.0 / 54.0     , -5.0 / 2.0    , -70.0 / 27.0    , -35.0 / 27.0       ,
		1631.0 / 55296.0 , 175.0 / 512.0 , 575.0 / 13824.0 , 44275.0 / 110592.0 , 253.0 / 4096.0
	};
	const double C5[] = {
		37.0 / 378.0,
		0.0,
		250.0 / 621.0,
		125.0 / 594.0,
		0.0,
		512.0 / 1771.0
	};
	const double C4[] = {
		2825.0 / 27648.0,
		0.0,
		18575.0 / 48384.0,
		13525.0 / 55296.0,
		277.0 / 14336.0,
		1.0 / 4.0
	};

	return KerrNewman::raw_adaptive_RK_step<6>(
		A, C4, C5,
		bh, mc, pc,
		pos,
		v_r, v_theta,
		h,
		abs_tol,
		accept_mult, reject_mult
	);

}

CUDA_HOST_DEVICE
bool KerrNewman::adaptive_DP45_step(
	KerrNewman::BlackHole const& bh,
	KerrNewman::MotionConstants const& mc,
	KerrNewman::ParticleConstants const& pc,
	Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult,
	double reject_mult
) {

	// butcher tableau
	const double A[] = {
		1.0 / 5.0,
		3.0 / 40.0       , 9.0 / 40.0        ,
		44.0 / 45.0      , -56.0 / 15.0      , 32.0 / 9.0       ,
		19372.0 / 6561.0 , -25360.0 / 2187.0 , 64448.0 / 6561.0 , -212.0 / 729.0 ,
		9017.0 / 3168.0  , -355.0 / 33.0     , 46732.0 / 5247.0 , 49.0 / 176.0   , -5103.0 / 18656.0 ,
		35.0 / 384.0     , 0.0               , 500.0 / 1113.0   , 125.0 / 192.0  , -2187.0 / 6784.0  , 11.0 / 84.0
	};
	const double C5[] = {
		35.0 / 384.0,
		0.0,
		500.0 / 1113.0,
		125.0 / 192.0,
		-2187.0 / 6784.0,
		11.0 / 84.0,
		0.0
	};
	const double C4[] = {
		5179.0 / 57600.0,
		0.0,
		7571.0 / 16695.0,
		393.0 / 640.0,
		-92097.0 / 339200.0,
		187.0 / 2100.0,
		1.0 / 40.0
	};

	return KerrNewman::raw_adaptive_RK_step<7>(
		A, C4, C5,
		bh, mc, pc,
		pos,
		v_r, v_theta,
		h,
		abs_tol,
		accept_mult, reject_mult
	);

}