//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#ifndef CURRAY2_KERRNEWMAN_METRIC_H
#define CURRAY2_KERRNEWMAN_METRIC_H

#include "default.h"

#include <simpleVec/vector.h>

namespace cuRRay2 {
namespace KerrNewman {

struct BlackHole {
	double M;	// mass
	double a;	// spin parameter
	double Q;	// electric charge
};

struct MetricComponents {
	double g00;
	double g11;
	double g22;
	double g33;
	double g03;
};

CUDA_HOST_DEVICE
SimpleVec::Vector4_double bl_to_cart(
	BlackHole const& bh,
	SimpleVec::Vector4_double const& bl_pos
);

CUDA_HOST_DEVICE
void compute_metric_components(
	BlackHole const& bh,
	SimpleVec::Vector4_double const& pos,
	MetricComponents* g
);

CUDA_HOST_DEVICE
double compute_outer_horizon_r(
	BlackHole const& bh
);

CUDA_HOST_DEVICE
double compute_norm_2(
	MetricComponents const& g,
	SimpleVec::Vector4_double const& vec
);

CUDA_HOST_DEVICE
SimpleVec::Vector4_double compute_covector(
	MetricComponents const& g,
	SimpleVec::Vector4_double const& vec
);

CUDA_HOST_DEVICE
double normalize_t_component(
	SimpleVec::Vector4_double const& vec,
	MetricComponents const& g,
	double norm2
);

struct ParticleConstants {
	double mu;	// rest mass
	double e;	// electric charge
};
const static ParticleConstants PHOTON = ParticleConstants{ 0.0, 0.0 };

struct MotionConstants {
	double E;	// energy at infinity
	double Lz;	// azimuthal angular momentum at infinity
	double C;	// Carter's constant
};

CUDA_HOST_DEVICE
void compute_motion_constants(
	BlackHole const& bh,
	double r, double theta,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double const& p_cov,
	MotionConstants* mc
);

CUDA_HOST_DEVICE
void compute_RHS(
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	double r, double theta,
	double v_r, double v_theta,
	double* v_t, double* v_phi,
	double* v_r2, double* v_theta2,
	double* a_r, double* a_theta
);

struct ChristoffelSymbols_0 {
	double G001;
	double G013;
	double G020;
	double G032;
};

struct ChristoffelSymbols_1 {
	double G100;
	double G103;
	double G111;
	double G112;
	double G122;
	double G133;
};

struct ChristoffelSymbols_2 {
	double G200;
	double G203;
	double G211;
	double G212;
	double G222;
	double G233;
};

struct ChristoffelSymbols_3 {
	double G301;
	double G302;
	double G313;
	double G332;
};

CUDA_HOST_DEVICE
void compute_christoffel_symbols(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_0* cs0,
	ChristoffelSymbols_1* cs1,
	ChristoffelSymbols_2* cs2,
	ChristoffelSymbols_3* cs3
);

CUDA_HOST_DEVICE
void compute_christoffel_symbols_0(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_0* cs0
);

CUDA_HOST_DEVICE
void compute_christoffel_symbols_1(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_1* cs1
);

CUDA_HOST_DEVICE
void compute_christoffel_symbols_2(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_2* cs2
);

CUDA_HOST_DEVICE
void compute_christoffel_symbols_3(
	BlackHole const& bh,
	double r, double theta,
	ChristoffelSymbols_3* cs3
);

}
}

#endif