//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

namespace cuRRay2 {
namespace KerrNewman {

template<unsigned stages>
CUDA_HOST_DEVICE
bool raw_adaptive_RK_step(
	double const* A, double const* C_low, double const* C_high,
	KerrNewman::BlackHole const& bh,
	KerrNewman::MotionConstants const& mc,
	KerrNewman::ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol, double accept_mult, double reject_mult
) {
	// memory used by substeps
	SimpleVec::Vector4_double v[stages];
	double a_r[stages];
	double a_theta[stages];

	double dummy;

	// first stage is always the same
	v[0].m_1 = *v_r;
	v[0].m_2 = *v_theta;
	KerrNewman::compute_RHS(
		bh, mc, pc,
		pos->m_1, pos->m_2,
		v[0].m_1, v[0].m_2,
		&v[0].m_0, &v[0].m_3,
		&dummy, &dummy,
		&a_r[0], &a_theta[0]
	);

	// loop through the rest of the stages
	unsigned index = 0;
	for (unsigned i = 0; i < stages - 1; i++) {

		// compute advancements of the coordinates and velocities for the current substep
		double adv_r = 0.0;
		double adv_theta = 0.0;
		double adv_v_r = 0.0;
		double adv_v_theta = 0.0;

		for (unsigned j = 0; j < i; j++) {
			adv_r += A[index] * v[j].m_1;
			adv_theta += A[index] * v[j].m_2;
			adv_v_r += A[index] * a_r[j];
			adv_v_theta += A[index] * a_theta[j];

			index++;
		}
		adv_r *= *h;
		adv_theta *= *h;
		adv_v_r *= *h;
		adv_v_theta *= *h;

		// do substep
		v[i + 1].m_1 = *v_r + adv_v_r;
		v[i + 1].m_2 = *v_theta + adv_v_theta;
		KerrNewman::compute_RHS(
			bh, mc, pc,
			pos->m_1 + adv_r,
			pos->m_2 + adv_theta,
			v[i + 1].m_1, v[i + 1].m_2,
			&v[i + 1].m_0, &v[i + 1].m_3,
			&dummy, &dummy,
			&a_r[i + 1], &a_theta[i + 1]
		);

	}

	// combine substeps
	SimpleVec::Vector4_double delta_pos = { 0.0, 0.0, 0.0, 0.0 };
	SimpleVec::Vector4_double low_delta_pos = { 0.0, 0.0, 0.0, 0.0 };
	double delta_v_r = 0.0;
	double delta_v_theta = 0.0;

	for (unsigned i = 0; i < stages; i++) {
		delta_pos += C_high[i] * v[i];
		low_delta_pos += C_low[i] * v[i];

		delta_v_r += C_high[i] * a_r[i];
		delta_v_theta += C_high[i] * a_theta[i];
	}

	delta_pos *= *h;
	low_delta_pos *= *h;
	delta_v_r *= *h;
	delta_v_theta *= *h;

	// check step
	unsigned acceptStep = (unsigned)KerrNewman::cart_check_adaptive_RK_step(
		bh,
		*pos, delta_pos, low_delta_pos,
		abs_tol
	);

	// execute step if accepted
	*h *= acceptStep * accept_mult
		+ (1 - acceptStep) * reject_mult;
	*pos += acceptStep * delta_pos;
	*v_r += acceptStep * delta_v_r;
	*v_theta += acceptStep * delta_v_theta;

	return acceptStep;
}

}
}