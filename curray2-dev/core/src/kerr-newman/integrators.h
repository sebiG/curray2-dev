//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#ifndef CURRAY2_KERRNEWMAN_INTEGRATORS_H
#define CURRAY2_KERRNEWMAN_INTEGRATORS_H

#include "default.h"
#include "kerr-newman/metric.h"

namespace cuRRay2 {
namespace KerrNewman {

/*
 * heuristic step size estimate (Psaltis & Johannsen 2012)
 */
constexpr double BASIC_STEP_SIZE_MULTIPLIER = 1.0 / 32.0;

CUDA_HOST_DEVICE
double heuristic_step_size_estimator(
	SimpleVec::Vector4_double const& pos,
	SimpleVec::Vector4_double const& vel
);

/*
 * Runge-Kutta 4 with heuristic step size estimate
 */
CUDA_HOST_DEVICE
double heuristic_RK4_step(
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta
);

/*
 * step rejection algorithm for adaptive Runge Kutta methods
 * performs evaluation in 4d cartesian coordinates
 *
 * returns true of accepted, false if rejected
 */
CUDA_HOST_DEVICE
bool cart_check_adaptive_RK_step(
	BlackHole const& bh,
	SimpleVec::Vector4_double const& pos,
	SimpleVec::Vector4_double const& delta_pos,
	SimpleVec::Vector4_double const& low_delta_pos,
	double abs_tol, double rel_tol = 0.0
);

/*
 * generic adaptive Runge Kutta method
 *
 * returns tue if step was accepted, false if rejected
 */
template<unsigned stages>
CUDA_HOST_DEVICE
bool raw_adaptive_RK_step(
	double const* A, double const* C_low, double const* C_high,
	KerrNewman::BlackHole const& bh,
	KerrNewman::MotionConstants const& mc,
	KerrNewman::ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol, double accept_mult, double reject_mult
);

constexpr static double DEFAULT_ACCEPT_MULTIPLIER = 1.5;
constexpr static double DEFAULT_REJECT_MULTIPLIER = 0.1;

/*
 * all implemented adaptive Runge Kutta methods
 */

enum Adaptive_RK_Integrators {
	EH12 = 0,
	BS23 = 1,
	RKF45 = 2,
	CK45 = 3,
	DP45 = 4
};
struct Adaptive_RK_Integrator_Description {
	std::string short_name;
	std::string long_name;
};
Adaptive_RK_Integrator_Description const adaptive_RK_integrators[] = {
	{ "EH12", "Euler-Heun (1/2)" },
	{ "BS23", "Bogacki-Shampine (2/3)" },
	{ "RKF45", "Runge-Kutta-Fehlberg (4/5)" },
	{ "CK45", "Cash-Karp (4/5)" },
	{ "DP45", "Dormand-Prince(4/5)" }
};
unsigned long const adaptive_RK_integrators_count = 
	sizeof(adaptive_RK_integrators)
	/ sizeof(Adaptive_RK_Integrator_Description);

#ifndef __CUDA_ARCH__

std::string print_adaptive_integrator_short_names();

bool adaptive_RK_step(
	unsigned method,
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult = DEFAULT_ACCEPT_MULTIPLIER,
	double reject_mult = DEFAULT_REJECT_MULTIPLIER
);

#endif

/*
 * adaptive Euler-Heun (RK of order 1/2)
 */
CUDA_HOST_DEVICE
bool adaptive_EH12_step(
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult = DEFAULT_ACCEPT_MULTIPLIER,
	double reject_mult = DEFAULT_REJECT_MULTIPLIER
);

/*
 * adaptive Bogacki–Shampine (RK of order 2/3)
 */
CUDA_HOST_DEVICE
bool adaptive_BS23_step(
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult = DEFAULT_ACCEPT_MULTIPLIER,
	double reject_mult = DEFAULT_REJECT_MULTIPLIER
);

/*
 * adaptive Runge-Kutta-Fehlberg (RK of order 4/5)
 */
CUDA_HOST_DEVICE
bool adaptive_RKF45_step(
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double *h,
	double abs_tol,
	double accept_mult = DEFAULT_ACCEPT_MULTIPLIER,
	double reject_mult = DEFAULT_REJECT_MULTIPLIER
);

/*
 * adaptive Cash-Karp (RK of order 4/5)
 */
CUDA_HOST_DEVICE
bool adaptive_CK45_step(
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult = DEFAULT_ACCEPT_MULTIPLIER,
	double reject_mult = DEFAULT_REJECT_MULTIPLIER
);

/*
 * adaptive Dormand-Prince (RK of order 4/5)
 */
CUDA_HOST_DEVICE
bool adaptive_DP45_step(
	BlackHole const& bh,
	MotionConstants const& mc,
	ParticleConstants const& pc,
	SimpleVec::Vector4_double* pos,
	double* v_r, double* v_theta,
	double* h,
	double abs_tol,
	double accept_mult = DEFAULT_ACCEPT_MULTIPLIER,
	double reject_mult = DEFAULT_REJECT_MULTIPLIER
);

}
}

// implementations of templates
#include "integrators_tmpl.h"

#endif
