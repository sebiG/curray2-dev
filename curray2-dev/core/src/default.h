//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#ifndef CURRAY2_DEFAULT_H
#define CURRAY2_DEFAULT_H

#define SIMPLEVEC_USE_ONLY_VECTOR4

// CUDA-specific
#if USE_CUDA

#define SIMPLEVEC_USE_CUDA

#include <cuda_runtime.h>
#define CUDA_HOST_DEVICE __host__ __device__

#else

#define CUDA_HOST_DEVICE

#endif

// Windows-specific
#ifdef _WIN64
#define UNICODE
#define NOMINMAX
#endif

// C++ standard types
#include <cstdint>

#endif