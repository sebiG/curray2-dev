//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#include "util/math.h"
#include "kerr-newman/metric.h"

#include <iostream>

using namespace cuRRay2;

int main(int argc, char** argv) {
	/*
	 * compute initial conditions for some photon orbits                        
	 */

	// always use a normalized, extremal Kerr-black-hole
	KerrNewman::BlackHole const bh = { 1.0, 1.0, 0.0 };

	// all the motion constants and radii from the orbits Teo (2003)
	KerrNewman::MotionConstants const mc[] = {
		{ 1.0, 0.0, 11.0 + 8.0 * sqrt(2.0) },
		{ 1.0, -1.0, 12.0 + 8.0 * sqrt(3.0) },
		{ 1.0, -2.0, 27.0 },
		{ 1.0, -6.0, -13.0 + 16.0 * sqrt(2.0) },
		{ 1.0, 1.0, 16.0 },
		{ 1.0, 1.999, 3.2590 }
	};
	double const r0[] = {
		1.0 + sqrt(2.0),
		1.0 + sqrt(3.0),
		3.0,
		1.0 + 2.0 * sqrt(2.0),
		2.0,
		1.0316
	};
	double const theta0 = MATH_PI_OVER_TWO;
	unsigned const ORBIT_COUNT = 6;

	// compute initial velocities
	for (unsigned i = 0; i < ORBIT_COUNT; ++i) {
		double v_t0, v_r0, v_theta0, v_phi0;
		double dummy;

		KerrNewman::compute_RHS(
			bh, mc[i], KerrNewman::PHOTON,
			r0[i], theta0,
			0.0, 0.0, /* we don't care about these values */
			&v_t0, &v_phi0,
			&v_r0, &v_theta0,
			&dummy, &dummy
		);
		v_r0 = sqrt(abs(v_r0));
		v_theta0 = sqrt(abs(v_theta0));

		std::cout << "orbit " << i << std::endl;

		std::cout << "E = " << mc[i].E << std::endl;
		std::cout << "Lz = " << mc[i].Lz << std::endl;
		std::cout << "C = " << mc[i].C << std::endl;
		std::cout << "r0 = " << r0[i] << std::endl;

		std::cout << "v_t0 = " << v_t0 << std::endl;
		std::cout << "v_r0 = " << v_r0 << std::endl;
		std::cout << "v_theta0 = " << v_theta0 << std::endl;
		std::cout << "v_phi0 = " << v_phi0 << std::endl;
		std::cout << std::endl;
	}
	
}