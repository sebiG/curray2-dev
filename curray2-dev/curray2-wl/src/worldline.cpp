//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#include "worldline.h"

#include <simpleVec/vector.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

#include "util/math.h"
#include "kerr-newman/metric.h"
#include "kerr-newman/integrators.h"

using namespace SimpleVec;
using namespace cuRRay2;

enum class IntegrationStopReason {
	HORIZON,
	SKY,
	N_MAX,
	T_MAX,
	L_MAX
};

void cuRRay2::print_launch_data(LaunchData const& ld) {
 
	std::cout << "initial position: " << ld.initial_pos << '\n';
	std::cout << "initial velocity: " << ld.initial_vel << '\n';
	std::cout << "black hole      : "
		<< ld.bh.M << ", " << ld.bh.a << ", " << ld.bh.Q << '\n';
	std::cout << "particle consts.: " << ld.pc.mu << ", " << ld.pc.e << '\n';
	std::cout << "integrator      : "
		<< KerrNewman::adaptive_RK_integrators[ld.integrator].long_name
		<< '\n';
	std::cout << "tolerance       : " << ld.tol << '\n';
	std::cout << "epsilon         : " << ld.epsilon << '\n';
	std::cout << "sky             : " << ld.sky << '\n';
	std::cout << "n_max           : " << ld.n_max << '\n';
	std::cout << "t_max           : " << ld.t_max << '\n';
	std::cout << "l_max           : " << ld.l_max << '\n';
	std::cout << "outfile         : " << ld.outfile << '\n';
	std::cout << "quiet           : " << ld.quiet << '\n';
	std::cout << "use_stdout      : " << ld.use_stdout << '\n';
	std::cout << std::flush;

}

int cuRRay2::trace_worldline(LaunchData const& ld) {
	
	Vector4_double initial_pos = ld.initial_pos;
	Vector4_double initial_vel = ld.initial_vel;

	KerrNewman::BlackHole bh = ld.bh;
	KerrNewman::ParticleConstants pc = ld.pc;

	// get black hole event horizon radius
	double horizon_r = KerrNewman::compute_outer_horizon_r(bh);

	// get metric components at initial position
	KerrNewman::MetricComponents g;
	KerrNewman::compute_metric_components(ld.bh, ld.initial_pos, &g);
	
	// normalize time component of initial velocity
	initial_vel.m_0 = KerrNewman::normalize_t_component(
		initial_vel, g,
		sgn(-pc.mu)
	);
	double initial_vel_norm2 = KerrNewman::compute_norm_2(g, initial_vel);

	// compute covariant momentum
	Vector4_double p_cov = KerrNewman::compute_covector(g, initial_vel);
	if (abs(pc.mu) > 0.0) {
		p_cov *= pc.mu;
	}

	// compute motion constants
	KerrNewman::MotionConstants mc;
	KerrNewman::compute_motion_constants(
		bh,
		initial_pos.m_1, initial_pos.m_2,
		pc,
		p_cov,
		&mc
	);
	
	// print information
	if (!ld.quiet) {
		std::cout << "event horizon r   : " << horizon_r << '\n';
		std::cout << "initial velocity  : " << initial_vel << '\n';
		std::cout << "initial vel. norm : " << initial_vel_norm2 << '\n';
		std::cout << "covariant momentum: " << p_cov << '\n';
		std::cout << "energy            : " << mc.E << '\n';
		std::cout << "z-angular momentum: " << mc.Lz << '\n';
		std::cout << "Carter's constant : " << mc.C << '\n';
		std::cout << "integrator        : "
			<< KerrNewman::adaptive_RK_integrators[ld.integrator].long_name
			<< '\n';
		std::cout << '\n';
		std::cout << "starting trace..." << '\n';
		std::cout << std::flush;
	}

	// required loop variables
	IntegrationStopReason stop_reason;

	std::ostringstream output;

	Vector4_double pos = initial_pos;
	double v_r = initial_vel.m_1;
	double v_theta = initial_vel.m_2;

	unsigned step_count = 0;
	unsigned rejected_count = 0;
	
	double delta_l = 0.0;
	
	double h = 1e-3;

	// open output file
	std::ofstream of;
	if (!ld.outfile.empty()) {
		of.open(ld.outfile);
		if (!of) {
			std::cerr << "Error opening file \"" << ld.outfile << "\""
				<< " for writing. Aborting." << std::endl;
			return 1;
		}
	}

	// write header
	output << "[\n";
	output << "[ \"t\", \"r\", \"theta\", \"phi\", \"v_t\", \"v_r\","
		<< "\"v_theta\", \"v_phi\", \"a_r\", \"a_theta\" ]";

	if (!ld.outfile.empty()) of << output.str();
	if (ld.use_stdout) std::cout << output.str();

	// timing
	auto time_start = std::chrono::high_resolution_clock::now();

	for (;; ++step_count) {
		// output current step
		{
			// get RHS for completeness' sake
			double v_t; double v_phi;
			double v_r2; double v_theta2;
			double a_r; double a_theta;
			KerrNewman::compute_RHS(
				bh, mc, pc,
				pos.m_1, pos.m_2,
				v_r, v_theta,
				&v_t, &v_phi,
				&v_r2, &v_theta2,
				&a_r, &a_theta
			);

			output.str("");
			output << ",\n[ ";
			output << pos.m_0 << " , " << pos.m_1
				<< " , " << pos.m_2 << " , " << pos.m_3;
			output << " , " << v_t << " , " << v_r
				<< " , " << v_theta << " , " << v_phi;
			output << " , " << a_r << " , " << a_theta << " ]";

			if (!ld.outfile.empty()) {
				of << output.str();
			}
			if (ld.use_stdout) {
				std::cout << output.str();
			}
		}

		// check if maximum step counts is reached
		if (ld.n_max > 0 && step_count >= ld.n_max) {
			stop_reason = IntegrationStopReason::N_MAX;
			break;
		}

		// check if maximum coordinate time is reached
		if (ld.t_max > 0.0 && abs(pos.m_0 - initial_pos.m_0) >= ld.t_max) {
			stop_reason = IntegrationStopReason::T_MAX;
			break;
		}

		// check if maximum affine parameter delta is reached
		if (ld.l_max > 0.0 && delta_l >= ld.l_max) {
			stop_reason = IntegrationStopReason::L_MAX;
			break;
		}

		// check if too close to the horizon
		if (pos.m_1 - ld.epsilon * horizon_r <= horizon_r) {
			stop_reason = IntegrationStopReason::HORIZON;
			break;
		}

		// check if too far away
		if (pos.m_1 >= ld.sky) {
			stop_reason = IntegrationStopReason::SKY;
			break;
		}

		// save step size for later
		double current_step_size = h;

		// do step
		bool accepted = KerrNewman::adaptive_RK_step(
			ld.integrator,
			bh, mc, pc,
			&pos, &v_r, &v_theta,
			&h,
			ld.tol
		);

		if (accepted) {
			delta_l += current_step_size;
		}
		else {
			++rejected_count;
		}
	}

	// timing
	auto time_stop = std::chrono::high_resolution_clock::now();
	unsigned long long elapsed_microseconds = 
		std::chrono::duration_cast<std::chrono::microseconds>
		(time_stop - time_start).count();
	double elapsed_seconds = (double)elapsed_microseconds * 1e-6;

	// close data output
	output.str("");
	output << "\n]";
	if (!ld.outfile.empty()) {
		of << output.str();
		of.close();
	}
	if (ld.use_stdout) {
		of << output.str();
	}

	// print information
	if (!ld.quiet) {
		std::cout << "tracing took " << elapsed_seconds << " seconds\n";
		std::cout << "step count    : " << step_count << '\n';
		std::cout << "rejected steps: " << rejected_count << '\n';
		std::cout << "delta t       : " << pos.m_0 - initial_pos.m_0 << '\n';
		std::cout << "delta param.  : " << delta_l << '\n';
		std::cout << "stop reason   : ";
		switch (stop_reason) {
		case IntegrationStopReason::HORIZON:
			std::cout << "horizon reached";
			break;
		case IntegrationStopReason::SKY:
			std::cout << "reached maximum r";
			break;
		case IntegrationStopReason::N_MAX:
			std::cout << "reached max. step count";
			break;
		case IntegrationStopReason::T_MAX:
			std::cout << "reached max. coordinate time delta";
			break;
		case IntegrationStopReason::L_MAX:
			std::cout << "reached max. affine parameter delta";
			break;
		}
		std::cout << std::endl;
	}

	return 0;
}
