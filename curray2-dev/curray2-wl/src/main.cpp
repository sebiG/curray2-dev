//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#include "default.h"

#include <tclap/CmdLine.h>
#include <simpleVec/vector.h>
#include <iostream>
#include <cstring>

#include "util/splash.h"
#include "util/math.h"
#include "kerr-newman/integrators.h"

#include "worldline.h"

using namespace SimpleVec;
using namespace cuRRay2;

// prints a banner
void print_banner() {
	std::cout << '\n';

	std::cout << cuRRay2::HLINE << '\n';
	std::cout << cuRRay2::BANNER << '\n';
	std::cout << cuRRay2::HLINE << '\n';
	std::cout << "                      worldline tool\n";
	std::cout << cuRRay2::HLINE << '\n';

	std::cout << std::endl;
}

// used to overwrite default TCLAP output
class CustomOutput : public TCLAP::StdOutput {
	virtual void usage(TCLAP::CmdLineInterface& c) {
		print_banner();
		TCLAP::StdOutput::usage(c);
		std::cout << std::flush;
	}
	virtual void failure(TCLAP::CmdLineInterface& c, TCLAP::ArgException& e) {
		print_banner();
		TCLAP::StdOutput::failure(c, e);
		std::cout << std::flush;
	}
};

// used to read vecotr components from a stream
template<typename T, unsigned N>
class VectorParser {
public:
	typedef TCLAP::ValueLike ValueCategory;

	T values[N];

	friend std::istream& operator>>(std::istream& is, VectorParser<T, N>& vp) {
		for (unsigned i = 0; i < N; i++) {
			if (!(is >> vp.values[i])) goto fail;
		}
		goto end;
	fail:
		is.setstate(std::ios::failbit);
	end:
		return is;
	}
};

int main(int argc, char** argv) {
	try {

		TCLAP::CmdLine cmd_line("", ' ', cuRRay2::VERSION);

		CustomOutput custom_output;
		cmd_line.setOutput(&custom_output);

		TCLAP::ValueArg<VectorParser<double, 4>> pos_arg(
			"p", "pos",
			"Initial particle position. The four coordinates should be" 
			" seperated by spaces and enclosed in quotes,"
			" e.g. \"0 4 1.571 0\".",
			true, { {0, 0, 0, 0} }, "BL coords."
		);
		TCLAP::ValueArg<VectorParser<double, 3>> vel_arg(
			"v", "vel",
			"Spatial part of initial particle velocity. The three components"
			" should be separated by spaces and enclosed in quotes,"
			" e.g. \"0 0 0\"",
			true, { {0, 0, 0} }, "spatial BL coords."
		);

		TCLAP::ValueArg<double> mass_arg(
			"M", "mass", 
			"Black hole mass. Defaults to 0.",
			false, 0.0, "decimal"
		);
		TCLAP::ValueArg<double> spin_arg(
			"a", "spin",
			"Black hole spin parameter. Defaults to 0.",
			false, 0.0, "decimal"
		);
		TCLAP::ValueArg<double> charge_arg("Q", "charge",
			"Black hole charge. Defaults to 0.",
			false, 0.0, "decimal"
		);

		TCLAP::ValueArg<double> p_m_arg(
			"m", "p_mass",
			"Particle mass. Defaults to 0.",
			false, 0.0, "decimal"
		);
		TCLAP::ValueArg<double> p_q_arg(
			"q", "p_charge",
			"Particle charge. Defaults to 0.",
			false, 0.0, "decimal"
		);

		TCLAP::ValueArg<std::string> integrator_arg(
			"i", "integrator",
			"The integrator to use. Allowed values: "
			+ KerrNewman::print_adaptive_integrator_short_names()
			+ ". Uses \"DP45\" by default.",
			false, "DP45", "integrator"
		);
		TCLAP::ValueArg<double> tol_arg(
			"t", "tol",
			"Tolerance used internally by the adaptive integrator. Understood"
			" in cartesian coordnates. Defaults to 1e-4.",
			false, 1e-4, "pos. decimal"
		);

		TCLAP::ValueArg<unsigned> n_arg(
			"n", "n_max",
			"Maximum number of integration steps. If not set, potentially"
			" infinitely many are allowed.",
			false, 0, "pos. integer"
		);
		TCLAP::ValueArg<double> tmax_arg(
			"", "t_max",
			"Stops integration after a certain amount of coordinate time" 
			" elapsed. If not set, time is not used as a stopping condition.",
			false, -1.0, "pos. decimal"
		);
		TCLAP::ValueArg<double> lmax_arg(
			"", "l_max",
			"Stops integration after the affine parameter (~proper time for"
			" timelike geodesics) reaches a certain value. If not set, the"
			" affine parameter is not used as a stopping condition.",
			false, -1.0, "pos. decimal"
		);
		TCLAP::ValueArg<double> epsilon_arg(
			"e", "epsilon",
			"Itegration stops at r < epsilon * (outer horizon radius). epsilon"
			" defaults to 1e-3.",
			false, 1e-3, "pos. decimal"
		);
		TCLAP::ValueArg<double> sky_arg(
			"s", "sky",
			"Sets the r integration limit. Defaults to 100.",
			false, 100.0, "pos. decimal"
		);

		TCLAP::ValueArg<std::string> outfile_arg(
			"f", "outfile",
			"Output file name.",
			false, "", "filename"
		);
		TCLAP::SwitchArg quiet_arg(
			"", "quiet",
			"Hides most stdout output. Errors are still reported via stderr."
			" Also the computed worldline data is written to stdout, if" 
			" --stdout is set.",
			false
		);
		TCLAP::SwitchArg stdout_arg(
			"", "stdout",
			"Outputs computed worldline data to stdout. Combine with --quiet"
			" for cleanest result.",
			false
		);

		cmd_line.add(quiet_arg);
		cmd_line.add(stdout_arg);
		cmd_line.add(outfile_arg);

		cmd_line.add(sky_arg);
		cmd_line.add(epsilon_arg);
		cmd_line.add(lmax_arg);
		cmd_line.add(tmax_arg);
		cmd_line.add(n_arg);

		cmd_line.add(tol_arg);
		cmd_line.add(integrator_arg);

		cmd_line.add(charge_arg);
		cmd_line.add(spin_arg);
		cmd_line.add(mass_arg);

		cmd_line.add(p_q_arg);
		cmd_line.add(p_m_arg);

		cmd_line.add(vel_arg);
		cmd_line.add(pos_arg);

		cmd_line.parse(argc, argv);

		// parse launch data from command line
		LaunchData launch_data;

		// read quiet arg and print banner accordingly
		launch_data.quiet = quiet_arg.getValue();
		if (!launch_data.quiet) print_banner();

		// parse other output options
		launch_data.outfile = outfile_arg.getValue();
		launch_data.use_stdout = stdout_arg.getValue();

		// parse initial data
		{
			VectorParser<double, 4> pos_parser = pos_arg.getValue();
			memcpy(launch_data.initial_pos.m_raw, pos_parser.values,
				4 * sizeof(double));

			VectorParser<double, 3> vel_parser = vel_arg.getValue();
			memcpy(&launch_data.initial_vel.m_raw[1], vel_parser.values,
				3 * sizeof(double));

			if (launch_data.initial_pos.m_1 < 0.0) {
				std::cerr << "\"" << launch_data.initial_pos.m_1 << "\""
					<< " is not a valid initial position r-coordinate."
					<< " It must be non-negative. Aborting." << std::endl;
				return 1;
			}
			if (
				launch_data.initial_pos.m_2 < 0.0 
				|| launch_data.initial_pos.m_2 > MATH_PI
			) {
				std::cerr << "\"" << launch_data.initial_pos.m_2 << "\""
					<< " is not a valid initial position theta-coordinate."
					<< " It must lie in the range [0, PI]. Aborting."
					<< std::endl;
				return 1;
			}
		}

		// parse black hole data
		{
			launch_data.bh.M = mass_arg.getValue();
			launch_data.bh.a = spin_arg.getValue();
			launch_data.bh.Q = charge_arg.getValue();
		}

		// parse particle constants
		{
			launch_data.pc.mu = p_m_arg.getValue();
			launch_data.pc.e = p_q_arg.getValue();
		}

		// parse integrator configuration
		{
			launch_data.integrator = -1;
			
			std::string integrator_str = integrator_arg.getValue();
			for (unsigned i = 0; 
				i < KerrNewman::adaptive_RK_integrators_count; ++i
			) {
				if (integrator_str
					== KerrNewman::adaptive_RK_integrators[i].short_name
				) {
					launch_data.integrator = i;
					break;
				}
			}
			if (launch_data.integrator == -1) {
				std::cerr << "\"" << integrator_str << "\""
					<< " is not a valid integrator; allowed values are "
					<< KerrNewman::print_adaptive_integrator_short_names()
					<< ". Aborting."
					<< std::endl;
				return 1;
			}

			launch_data.tol = tol_arg.getValue();
			if (launch_data.tol <= 0.0) {
				std::cerr << "\"" << launch_data.tol << "\""
					<< " is not a valid integrator tolerance."
					<< " It must be strictly positive. Aborting."
					<< std::endl;
				return 1;
			}

			launch_data.epsilon = epsilon_arg.getValue();
			if (launch_data.epsilon <= 0.0) {
				std::cerr << "\"" << launch_data.epsilon << "\""
					<< " is not a valid event horizon epsilon."
					<< " It must be strictly positive. Aborting."
					<< std::endl;
				return 1;
			}

			launch_data.sky = sky_arg.getValue();
			if (launch_data.sky <= 0.0) {
				std::cerr << "\"" << launch_data.sky << "\""
					<< " is not a valid r integration limit."
					<< " It must be strictly positive. Aborting."
					<< std::endl;
				return 1;
			}

			launch_data.n_max = 0;
			if (n_arg.isSet()) {
				launch_data.n_max = n_arg.getValue();
				if (launch_data.n_max == 0) {
					std::cerr << "\"" << launch_data.n_max << "\""
						<< " is not a valid value for n_max."
						<< " It must be greater than zero. Aborting"
						<< std::endl;
					return 1;
				}
			}

			launch_data.t_max = -1.0;
			if (tmax_arg.isSet()) {
				launch_data.t_max = tmax_arg.getValue();
				if (launch_data.t_max <= 0.0) {
					std::cerr << "\"" << launch_data.t_max << "\""
						<< " is not a valid value for t_max."
						<< " It must be strictly positive. Aborting"
						<< std::endl;
					return 1;
				}
			}

			launch_data.l_max = -1.0;
			if (lmax_arg.isSet()) {
				launch_data.l_max = lmax_arg.getValue();
				if (launch_data.l_max <= 0.0) {
					std::cerr << "\"" << launch_data.l_max << "\""
						<< " is not a valid value for l_max."
						<< " It must be strictly positive. Aborting"
						<< std::endl;
					return 1;
				}
			}
		}

		// test
		//print_launch_data(launch_data);

		// trace worldline
		return trace_worldline(launch_data);

	} catch (TCLAP::ArgException& e) {
		// This should never happen
		std::cout << "TCLAP::ArgException: " << e.error() + " for argument "
			<< e.argId() << std::endl;
		return 1;
	}

	return 0;
}
