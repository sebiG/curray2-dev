//
// This file is part of cuRRay 2.
//
// (c) 2020 Sebastien Garmier (kontakt@sebastiengarmier.ch)
//

#ifndef CURRAY2_WL_WORLDLINE_H
#define CURRAY2_WL_WORLDLINE_H

#include "default.h"

#include "kerr-newman/metric.h"
#include "kerr-newman/integrators.h"

#include <string>

#include <simpleVec/vector.h>

namespace cuRRay2 {

struct LaunchData {
	SimpleVec::Vector4_double initial_pos;
	// initially, the time component will not be set
	SimpleVec::Vector4_double initial_vel;

	KerrNewman::BlackHole bh;
	KerrNewman::ParticleConstants pc;

	int integrator;
	double tol;
	double epsilon;
	double sky;

	unsigned n_max;
	double t_max;
	double l_max;

	std::string outfile;
	bool quiet;
	bool use_stdout;
};

void print_launch_data(LaunchData const& ld);

int trace_worldline(LaunchData const& ld);

}

#endif